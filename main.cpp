#include <iostream>
#include <string>
#include <vector>
#include <chrono>
#include <thread>
#include <map>

#include "MPU.h"
#include "Peripheral.h"
#include "peripherals/Clock.h"
#include "peripherals/Timer.h"
#include "peripherals/RAM.h"
#include "peripherals/ROM.h"
#include "peripherals/LCD.h"
#include "peripherals/Interrupt.h"
#include "peripherals/Sound.h"
#include "peripherals/IOPort.h"

#include "SdlDisplay.h"

int main(int argc, char *argv[]) {
  MPU mpu;
  ROM rom(mpu, 20);
  RAM ram(mpu);
  Timer timers(mpu);
  SPU spu(mpu);
  Clock clock(mpu);
  Interrupt interrupt(mpu);
  IOPort io(mpu);
  LCD lcd(mpu);

  SDLDisplay display(lcd, io);

  std::cout << "-- Initializing Graphics" << std::endl;
  display.init();

  std::cout << "-- Initializing Simulation" << std::endl;
  mpu.reset();

  std::cout << "-- Starting emulation" << std::endl;

  bool exit = false;

  while (!exit) {
    mpu.beginRun();
    display.update();
    mpu.endRun();
  }

  return 0;
}
