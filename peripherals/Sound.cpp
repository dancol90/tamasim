//
// Created by Daniele Colanardi on 06/12/15.
//

#include "Sound.h"

SPU::SPU(MPU &mpu) : Peripheral(mpu, "SPU") { }


void SPU::reset() {}


bool SPU::checkAddress(uint16_t addr) {
  return  addr == 0x3006 ||
      (addr >= 0x3051 && addr <= 0x3065);
}


void SPU::write(uint16_t addr, uint8_t value) {}


uint8_t SPU::read(uint16_t addr) {
  return 0;
}

void SPU::update(unsigned int cycles) {}

