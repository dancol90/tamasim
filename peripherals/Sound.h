//
// Created by Daniele Colanardi on 06/12/15.
//

#ifndef TAMAGOTCHI_SOUND_H
#define TAMAGOTCHI_SOUND_H

#include "../MPU.h"
#include "../Peripheral.h"

class SPU : Peripheral {

public:
  SPU(MPU &mpu);

  bool checkAddress(uint16_t addr);

  void write(uint16_t addr, uint8_t value);

  uint8_t read(uint16_t addr);

  void update(unsigned int cycles);

  void reset();
};

#endif //TAMAGOTCHI_SOUND_H
