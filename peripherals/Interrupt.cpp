//
// Created by Daniele Colanardi on 30/11/15.
//

#include "Interrupt.h"

#define setLowByte(x, v)  x = ((x & 0xFF00) + v)
#define setHighByte(x, v) x = ((v << 8) + (x & 0x00FF))

#define getLowByte(x)  (uint8_t)(x & 0xFF)
#define getHighByte(x) (uint8_t)((x >> 8) & 0xFF)

#define isBitSet(x, b) (x & (1 << b))

const Interrupt::InterruptInfo Interrupt::_interrupts[] = {
  // IRQ flag bit, Wake flag bit, Vector address
  {  1,            -1,             0xFFCC}, // IRQ_SPI
  {  2,            -1,             0xFFCA}, // IRQ_SPU
  {  3,            -1,             0xFFC8}, // IRQ_8K
  {  4,            -1,             0xFFC6}, // IRQ_2K
  {  0,             6,             0xFFCE}, // IRQ_FP
  {  10,            1,             0xFFDA}, // IRQ_TBL
  {  11,            3,             0xFFD8}, // IRQ_TBH
  {  7,             2,             0xFFC0}, // IRQ_TM0
  {  13,            4,             0xFFD4}, // IRQ_TM1
  {  -1,            0,             0x0000}, // WAKE_PORTA
};

Interrupt::Interrupt(MPU &mpu) : InterruptPeripheral(mpu, "Interrupt") { }

void Interrupt::reset() {
  _irq_control = 0;
  _irq_flags = 0;
  _nmi_control = 0;

  _wake_control = 0;
  _wake_flags = 0;
}


bool Interrupt::checkAddress(uint16_t addr) {
  return (addr == 0x3004) || // Watchdog
      (addr >= 0x3007 && addr <= 0x3008) || // Wake registers
      (addr >= 0x3070 && addr <= 0x3076) || // Control registers
      (addr >= 0x3090 && addr <= 0x309D);   // Clear flag registers
}


void Interrupt::write(uint16_t addr, uint8_t value) {
  // Logging purpose
  //Peripheral::write(addr, value);

  if (addr == 0x3070)      setLowByte(_irq_control, value);
  else if (addr == 0x3071) setHighByte(_irq_control, value);
  else if (addr == 0x3073) setLowByte(_irq_flags, value);
  else if (addr == 0x3074) setHighByte(_irq_flags, value);
  else if (addr == 0x3076) _nmi_control = value;
  else if (addr == 0x3007) _wake_control = value;
  else if (addr == 0x3008) _wake_flags = value;
  else if (addr != 0x3004) printf("[Interrupt] Some clear flag written: 0x%04x\n", addr);
}


uint8_t Interrupt::read(uint16_t addr) {
  // Logging purpose
  //Peripheral::read(addr);

  if (addr == 0x3070)      return getLowByte(_irq_flags);
  else if (addr == 0x3071) return getHighByte(_irq_flags);
  else if (addr == 0x3076) return (uint8_t)_nmi_control;
  else if (addr == 0x3008) return (uint8_t)_wake_flags;

  return 0;
}


void Interrupt::update(unsigned int cycles) { }


void Interrupt::trigger(int irq, MPUInterruptType& type, unsigned int& vector) {
  InterruptInfo info = _interrupts[irq];

  type = INT_NONE;

  if (info.bit >= 0) {
    if (isBitSet(_irq_control, info.bit)) {
      type = INT_IRQ;
      vector = info.vector;

      _irq_flags |= 1 << info.bit;
    }
  }

  if (isBitSet(_nmi_control, 7) && irq == IRQ_TM1) {
    type = INT_NMI;
    vector = 0;
  }

  if (info.wake_bit >= 0) {
    // If the wake source is enabled and MPU is actually sleeping
    if (isBitSet(_wake_control, info.wake_bit) && _mpu.getDivider() == 0) {
      _wake_flags |= 1 << info.wake_bit;

      // Wake MPU!
      _mpu.setDivider(8);
    }
  }


}
