//
// Created by Daniele Colanardi on 30/11/15.
//

#include <fstream>

#include "ROM.h"

ROM::ROM(MPU &mpu, uint8_t file_count) : Peripheral(mpu, "ROM") {
  count = file_count;
  selected = 0;

  // Load roms here
  ifstream file;
  char file_name[50];
  streampos file_size;

  banks = new char*[count];

  for (int i = 0; i < count; i++) {
    banks[i] = new char[_BANK_SIZE];

    sprintf(file_name, "rom/p%d.bin", i);

    printf("[%s] Reading ROM bank %s...", _name.c_str(), file_name);

    file.open(file_name, ios::in | ios::binary | ios::ate);

    if(file.is_open()) {
      file_size = file.tellg();

      file.seekg(file_size > _BANK_SIZE ? _OFFSET : 0, ios::beg);
      file.read(banks[i], _BANK_SIZE);

      file.close();

      printf("OK\n");
    } else {
      printf("ERROR\n");
    }
  }
}


ROM::~ROM() {
  for (int i = 0; i < count; i++) {
    delete[] banks[i];
  }

  delete[] banks;
}


void ROM::reset() {
  selected = 0;
}


bool ROM::checkAddress(uint16_t addr) {
  return (addr == 0x3000) ||
      (addr >= 0x4000 && addr <= 0xffff);
}


void ROM::write(uint16_t addr, uint8_t value) {
  // Logging purpose
  //Peripheral::write(addr, value);

  if (addr == 0x3000)
    if (value < count)
      selected = value;
    else
      printf("[%s] Invalid bank selection: %d/%d", _name.c_str(), value, count);
  else
    printf("[%s] Invalid write on read-only memory: 0x%04x", _name.c_str(), addr);

}


uint8_t ROM::read(uint16_t addr) {
  // Logging purpose
  //Peripheral::read(addr);

  uint8_t ret;

  if (addr == 0x3000)
    ret = selected;
  else if (addr >= 0x4000 && addr < 0xC000)
    // Extended bank
    ret = (uint8_t) banks[selected][addr - 0x4000];
  else
    ret = (uint8_t) banks[0][addr - 0xC000];

  //printf("[%s] Value read: 0x%02x\n", _name.c_str(), ret);

  return ret;
}

void ROM::update(unsigned int cycles) {

}
