//
// Created by Daniele Colanardi on 09/12/15.
//

#include <iostream>
#include <fstream>
#include <cstring>

#include "EEPROM.h"

using namespace std;

EEPROM::EEPROM() {
  ifstream in_file;

  in_file.open("eeprom.bin", ios::in | ios::binary);

  if(in_file.is_open()) {
    //printf("[EEPROM] Reading from file\n");
    in_file.read((char *)_eeprom, _eeprom_size);
    in_file.close();
  } else {
    memset(_eeprom, 0xFF, _eeprom_size);
  }

  _addr_step = 2;

}

void EEPROM::onStart(bool write) {
  _addr_step = write ? 0 : 2;
}

void EEPROM::byteRecv(unsigned char value) {
  if (_addr_step == 0) {
    _curr_reg = (value & 0x7F) << 8;
    _addr_step++;
  } else if (_addr_step == 1) {
    _curr_reg |= value;
    _addr_step++;

    //printf("[EEPROM] Jump to register %d\n", _curr_reg);
  } else {
    //printf("[EEPROM] Writing to register %d value 0x%02x\n", _curr_reg, value);

    // Write to eeprom
    _eeprom[_curr_reg] = value;

    ofstream _out_file;
    _out_file.open("eeprom.bin", ios::out | ios::binary);
    _out_file.write((char*)_eeprom, _eeprom_size);
    _out_file.close();

    int page = _curr_reg & 0xFFE0;

    _curr_reg++;
    _curr_reg = page | (_curr_reg & 0x1F);

    if (_curr_reg > _eeprom_size) {
      printf("[EEPROM] End of eeprom\n");
    }
  }

}

unsigned char EEPROM::getByte() {
  //printf("[EEPROM] Reading from register %02x value 0x%02x\n", _curr_reg, _eeprom[_curr_reg]);

  unsigned char ret = _eeprom[_curr_reg];

  _curr_reg = (_curr_reg + 1) & 0xffff;

  // Return eeprom @ _curr_reg
  return ret;
}
