#include <iostream>

#include "I2CBus.h"
#include "EEPROM.h"

I2CBus::I2CBus() {
  // Temporary solution
  _device = new EEPROM();
}


I2CBus::~I2CBus() {
  // Temporary solution
  //delete _device;
}

void I2CBus::reset() {
  _recv_byte = 0;
  _recv_count = 0;

  _send_byte = 0;
  _send_count = 0;

  _prev_clock = 0;
  _prev_data = 0;

  _state = I2C_IDLE;
}

bool I2CBus::readBit(bool clock, bool data) {
  // Data is ready to be read

  if (_recv_count < 8) {
    // Add the read bit at the correct position
    _recv_byte |= (data & 0x01) << (7 - _recv_count);
    _recv_count++;
  } else {
    // Set output bit to ACK
    out_bit = 0;

    _recv_count = 0;

    return true;
  }

  return false;
}

bool I2CBus::writeBit(bool clock) {
  // Data is ready to be written

  if (_send_count < 8) {
    out_bit = _send_byte & 0x80;
    _send_byte <<= 1;
    _send_count++;
  } else {
    _send_count = 0;
    return true;
  }

  return false;
}

void I2CBus::update(bool power, bool clock, bool data) {
  if (!power)
    return;

  if (clock == _prev_clock) {
    // Clock is not changed, could be a start/stop condition

    // Clock must be high to be a start/stop condition
    if (clock && data != _prev_data) {
      _state = data == 0 ? I2C_STARTED : I2C_IDLE;

      //printf("[I2C] %s signal received\n", data == 0 ? "Start" : "Stop");

      _recv_byte = 0;
      _recv_count = 0;

      _send_byte = 0;
      _send_count = 0;
    }

  } else if (clock) {
    // Clock rise (0->1) transition, normal op

    if (_state == I2C_STARTED) {
      // Waiting for a device address
      if (readBit(clock, data)) {
        //printf("[I2C] Master request is for 0x%02x\n", _recv_byte);

        // Received a byte.
        if ((_recv_byte & 0xFE) == _device->getAddress()) {
          // This message is for me.
          _state = _recv_byte & 0x01 ? I2C_READING : I2C_WRITING;
          _recv_byte = 0;

          _device->onStart(_state == I2C_WRITING);
        } else {
          _state = I2C_IDLE;
        }
      }
    } else if (_state == I2C_WRITING) {
      // Master is writing, data is an output pin on MPU

      // Read a bit of the register
      if (readBit(clock, data)) {
        //printf("[I2C] Slave got a byte: 0x%02x\n", _recv_byte);

        _device->byteRecv(_recv_byte);

        _recv_byte = 0;
      }
    } else if (_state == I2C_READING) {
      // Master is reading, data is an input pin on MPU

      if (_send_count == 0) {
        // get byte
        _send_byte = _device->getByte();

        //printf("[I2C] Slave sent a byte: 0x%02x\n", _send_byte);
      }

      if (writeBit(clock)) {
        // Whole byte has been written, wait for ACK
        _state = I2C_ACK_WAIT;
      }
    } else if (_state == I2C_ACK_WAIT) {
      // We have received an ACK from master.

      // Low ACK = more byte, High ACK = end please (go to idle waiting for stop condition)
      _state = data ? I2C_IDLE : I2C_READING;
    }
  }

  _prev_data = data;
  _prev_clock = clock;
}
