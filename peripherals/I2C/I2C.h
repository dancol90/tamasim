//
// Created by Daniele Colanardi on 09/12/15.
//

#ifndef TAMAGOTCHI_I2C_H_H
#define TAMAGOTCHI_I2C_H_H

class I2CDevice {
public:
    // I2C address, already shifted to leave space for r/w bit 0
    virtual int getAddress() = 0;

    // write = true if write to slave request, false if read from slave request
    virtual void onStart(bool write) = 0;
    virtual void byteRecv(unsigned char value) = 0;
    virtual unsigned char getByte() = 0;
};

#endif //TAMAGOTCHI_I2C_H_H
