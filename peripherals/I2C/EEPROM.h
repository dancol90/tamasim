//
// Created by Daniele Colanardi on 09/12/15.
//

#ifndef TAMAGOTCHI_EEPROM_H
#define TAMAGOTCHI_EEPROM_H

#include "I2C.h"

class EEPROM : public I2CDevice {
private:
  const unsigned int _eeprom_size = 256 * 1024 / 8;

  int _curr_reg;
  unsigned short _addr_step;
  unsigned char _eeprom[256 * 1024 / 8];

public:
  EEPROM();

  // I2C address, already shifted to leave space for r/w bit 0
  int getAddress() { return  0xA0; }

  // write = true if write to slave request, false if read from slave request
  void onStart(bool write);
  void byteRecv(unsigned char value);
  unsigned char getByte();
};


#endif //TAMAGOTCHI_EEPROM_H
