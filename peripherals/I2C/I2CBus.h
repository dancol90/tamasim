#ifndef TAMAGOTCHI_I2C_BUS_H
#define TAMAGOTCHI_I2C_BUS_H

#include "I2CDevice.h"

class I2CBus {
public:
  I2CBus();
  ~I2CBus();

  void reset();

  // data = 0:low, 1:high, 2:input
  void update(bool power, bool clock, bool data);

  bool out_bit;
private:
  enum I2CState { I2C_IDLE, I2C_STARTED, I2C_READING, I2C_WRITING, I2C_ACK_WAIT };

  bool readBit(bool clock, bool data);
  bool writeBit(bool clock);

  I2CState _state;

  bool _prev_clock, _prev_data;
  unsigned char _recv_byte, _recv_count;
  unsigned char _send_byte, _send_count;

  I2CDevice* _device;
};

#endif // TAMAGOTCHI_I2C_BUS_H
