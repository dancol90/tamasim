//
// Created by Daniele Colanardi on 06/12/15.
//

#ifndef TAMAGOTCHI_IOPORT_H
#define TAMAGOTCHI_IOPORT_H

#include "../MPU.h"
#include "../Peripheral.h"

#include "I2C/I2CBus.h"

class IOPort : Peripheral {

public:
  IOPort(MPU &mpu);

  bool checkAddress(uint16_t addr);

  void write(uint16_t addr, uint8_t value);
  uint8_t read(uint16_t addr);

  void update(unsigned int cycles);
  void reset();


  void setButton(unsigned int btn, bool pressed);

private:
  I2CBus _i2c;

  unsigned int _regs[7];

  // Special case: PortA read are different from write (pin status vs pullup)
  unsigned int _buttons_status;
};

#endif //TAMAGOTCHI_IOPORT_H
