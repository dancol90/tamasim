//
// Created by Daniele Colanardi on 05/12/15.
//

#ifndef TAMAGOTCHI_RAM_H
#define TAMAGOTCHI_RAM_H

#include "../MPU.h"
#include "../Peripheral.h"

class RAM : Peripheral {

public:
  RAM(MPU &mpu);

  bool checkAddress(uint16_t addr);

  void write(uint16_t addr, uint8_t value);
  uint8_t read(uint16_t addr);

  void update(unsigned int cycles);
  void reset();

private:
  uint8_t _ram[1536];

};


#endif //TAMAGOTCHI_RAM_H
