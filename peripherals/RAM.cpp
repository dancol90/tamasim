//
// Created by Daniele Colanardi on 05/12/15.
//

//
// Created by Daniele Colanardi on 30/11/15.
//

#include <memory.h>

#include "RAM.h"

RAM::RAM(MPU &mpu) : Peripheral(mpu, "RAM") { }


void RAM::reset() {
  memset(_ram, 0, 1536);
}


bool RAM::checkAddress(uint16_t addr) {
  return (addr >= 0x0000 && addr <= 0x0600);
}


void RAM::write(uint16_t addr, uint8_t value) {
  _ram[addr] = value;
}


uint8_t RAM::read(uint16_t addr) {
  // Logging purpose
  //Peripheral::read(addr);

  return _ram[addr];
}

void RAM::update(unsigned int cycles) { }
