//
// Created by Daniele on 04/12/2015.
//

#ifndef TAMAGOTCHI_INTERRUPT_H
#define TAMAGOTCHI_INTERRUPT_H

#include "../MPU.h"
#include "../Peripheral.h"

enum InterruptType {
  IRQ_SPI, IRQ_SPU, IRQ_8K, IRQ_2K,
  IRQ_FP, IRQ_TBL, IRQ_TBH, IRQ_TM0, IRQ_TM1,
  WAKE_PORTA // Special case: PortA pin change wakes MPU, does not generate an interrupt
};

class Interrupt : InterruptPeripheral {

public:
  Interrupt(MPU& mpu);

  bool checkAddress(uint16_t addr);

  void write(uint16_t addr, uint8_t value);
  uint8_t read(uint16_t addr);

  void update(unsigned int cycles);
  void reset();

  void trigger(int irq, MPUInterruptType& type, unsigned int& vector);

private:
  unsigned int _irq_control, _irq_flags;
  unsigned int _wake_control, _wake_flags;
  unsigned short _nmi_control;

  struct InterruptInfo {
    short bit;
    short wake_bit;
    unsigned int vector;
  };

  static const InterruptInfo _interrupts[];
};



#endif //TAMAGOTCHI_INTERRUPT_H
