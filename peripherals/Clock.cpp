//
// Created by Daniele Colanardi on 30/11/15.
//

#include "Clock.h"

Clock::Clock(MPU &mpu) : Peripheral(mpu, "Clock") {
  _32k_clock_enabled = 1;
}

void Clock::reset() {
  _mpu.setDivider(0x02);
}


bool Clock::checkAddress(uint16_t addr) {
  return (addr == 0x3001 || addr == 0x3002);
}


void Clock::write(uint16_t addr, uint8_t value) {
  // Logging purpose
  //Peripheral::write(addr, value);

  if (addr == 0x3001) {
    char i = (char) (value & 0x07);

    if (i == 7)
      _mpu.setDivider(0);
    else
      // Divider value is 2^(i+1)
      _mpu.setDivider((unsigned short)(2 << i));
  } else if (addr == 0x3002)
    _32k_clock_enabled = (bool)(value & 0x80);
}


uint8_t Clock::read(uint16_t addr) {
  // Logging purpose
  //Peripheral::read(addr);

  if (addr == 0x3001)
    return (uint8_t)_mpu.getDivider();
  else if (addr == 0x3002)
    return (uint8_t)(_32k_clock_enabled << 7);
  else
    return 0;
}


void Clock::update(unsigned int cycles) { }
