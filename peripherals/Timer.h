//
// Created by Daniele on 03/12/2015.
//

#ifndef TAMAGOTCHI_TIMER_H
#define TAMAGOTCHI_TIMER_H

#include "../MPU.h"
#include "../Peripheral.h"

class Timer : Peripheral {

public:
  Timer(MPU& mpu);

  bool checkAddress(uint16_t addr);
  void write(uint16_t addr, uint8_t value);
  uint8_t read(uint16_t addr);

  void update(unsigned int cycles);

  void reset();

private:
  class Counter {
  public:
    Counter(unsigned int target) : target(target), counter(0) {}

    void reset() { counter = 0; ticks = 0; }
    int feed(int cycles) {
      if (target == 0)
        return 0;

      counter += cycles;

      if (counter >= target) {
        // Timer overflow
        ticks = (unsigned int)(counter / target);

        counter = counter % target;
      } else {
        ticks = 0;
      }

      return ticks;
    }
    int tick(int ticks) { return feed(ticks * target); }

    unsigned int target;
    unsigned long counter;
    // Ticks in the last update
    unsigned int ticks;
  };

  unsigned const long TimeBaseLowDiv [4] = { 2,   8,   4,   16 };
  unsigned const long TimeBaseHighDiv[4] = { 128, 512, 256, 1024 };

  /*
     * Timer0 Sources are
     *   T0S_VSS, T0S_ROSC, T0S_32K, T0S_ECLK, T0S_VDD,
     *   T0S_TBL, T0S_TBH, T0S_EXTI, T0S_2HZ, T0S_8HZ, T0S_32HZ, T0S_64HZ
     * Timer1 Sources are
     *   T1S_VSS, T1S_ROSC, T1S_32K, T1S_T0
     */

  // Time base
  Counter _tbh, _tbl;

  // Timer 0 & 1
  Counter _timer0, _timer1;
  char _t0_source, _t1_source;

  // Fixed frequency timers
  Counter _t2, _t8, _t32, _t64, _t32k;
  Counter _t2k, _t8k;
};


#endif //TAMAGOTCHI_TIMER_H
