//
// Created by Daniele Colanardi on 06/12/15.
//

#include "IOPort.h"
#include "Interrupt.h"

/*
 * NOTE:
 * - 0x3013 PortA strobe set to 0 (not used)
 *
 * - 0x3010 PortA config set    0x88 (10001000) 1 for disabled/floating pin (IGNORED???)
 *
 * - 0x3011 PortA direction set 0x10 (00010000) In or output
 * - 0x3012 PortA data/wake set 0x1f (10001111) Pullup/down in or Low/High out
 *                                    IIIOIIII
 *                                    UDDLUUUU (D=pulldown, U=pullup, L=out low)
 *
 *
 * - 0x3014 PortB config to  0x01 (00000001) 1 for disabled/floating pin (IGNORED???)
 *
 * - 0x3015 PortB dir set to 0x7e (01111110) (1=out, 0=in)
 * - 0x3016 PortB data to    0x07 (00000111) Pullup/down in or Low/High out
 *                                 IOOOOOOI
 *                                 DLLLLHHU
 * */

/*
 * PORT A:
 *
 * |   7   |      6     |      5     |      4       |   3   |   2   |   1   |   0   |
 * | IR Rx | Figure CD2 | Figure CD1 | Figure Power | Reset |   C   |   B   |   A   |
 * | in/up |  in/down   |  in/down   |   out/low    | in/up | in/up | in/up | in/up |
 *
 * PORT B:
 *
 * |    7    |    6    |    5    |    4    |    3    |     2     |    1     |    0     |
 * | SPI Rx  | SPI Tx  | SPI Clk | SPI CSN |  IR Tx  | I2C Power | I2C Clk  | I2C Data |
 * | in/down | out/low | out/low | out/low | out/low |  out/high | out/high |  in/up   |
 *
 * */

IOPort::IOPort(MPU &mpu) : Peripheral(mpu, "IOPort") { }


void IOPort::reset() {
  _i2c.reset();
  // All 3 buttons pulled high
  _buttons_status = 0x15;
}


bool IOPort::checkAddress(uint16_t addr) {
  return (addr >= 0x3010 && addr <= 0x3016);
}


void IOPort::write(uint16_t addr, uint8_t value) {
  //printf("[IOPort] Writing to 0x%04x value 0x%02x\n", addr, value);

  _regs[addr - 0x3010] = value;

  // Update i2c here
  if (addr == 0x3016) {
    _i2c.update(value & 0x04, value & 0x02, value & 0x01);
  }
}


uint8_t IOPort::read(uint16_t addr) {
  //printf("[IOPort] Reading from 0x%04x\n", addr);

  unsigned int &reg = _regs[addr - 0x3010];

  if (addr == 0x3016) {
    // If requesting I2C bits, check if data bit is input:
    // if it is, set it to i2c.out_bit

    // If bit 0 is set as input (I2C data)
    if (!(_regs[5] & 0x01)) {
      return (uint8_t)((reg & 0xFE) | (_i2c.out_bit & 0x01));
    }
  } else if (addr == 0x3012) {
    return (uint8_t)((reg & 0xF8) | (_buttons_status & 0x07));
  }

  return (uint8_t)reg;
}

void IOPort::update(unsigned int cycles) { }


void IOPort::setButton(unsigned int btn, bool pressed) {
  //printf("[IOPort] Pressed button %d: before=%02x\n", btn, _regs[0x3012-0x3010]);
  //printf("[IOPort] Released button %d\n", btn);

  // Input pins are set as pulled-high, hence active-low.
  _buttons_status = (_buttons_status & ~(1 << btn)) | (!pressed << btn);
  _mpu.interrupt(WAKE_PORTA);
}
