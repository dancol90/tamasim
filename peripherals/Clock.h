//
// Created by Daniele Colanardi on 30/11/15.
//

#ifndef TAMAGOTCHI_CLOCK_H
#define TAMAGOTCHI_CLOCK_H

#include "../MPU.h"
#include "../Peripheral.h"

class Clock : Peripheral {

public:
  Clock(MPU& mpu);
  Clock(MPU& mpu, unsigned long fcpu);

  bool checkAddress(uint16_t addr);

  void write(uint16_t addr, uint8_t value);
  uint8_t read(uint16_t addr);

  void update(unsigned int cycles);
  void reset();

private:
  bool _32k_clock_enabled;
};


#endif //TAMAGOTCHI_CLOCK_H
