//
// Created by Daniele Colanardi on 30/11/15.
//

#ifndef TAMAGOTCHI_ROM_H
#define TAMAGOTCHI_ROM_H

#include "../MPU.h"
#include "../Peripheral.h"

class ROM : Peripheral {

public:
  ROM(MPU &mpu, uint8_t file_count);
  ~ROM();

  bool checkAddress(uint16_t addr);

  void write(uint16_t addr, uint8_t value);
  uint8_t read(uint16_t addr);

  void update(unsigned int cycles);
  void reset();

private:
  const int _BANK_SIZE = 32768;
  const int _OFFSET = 0x4000;

  uint8_t count, selected;
  char** banks;
};


#endif //TAMAGOTCHI_ROM_H
