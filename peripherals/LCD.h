//
// Created by Daniele Colanardi on 30/11/15.
//

#ifndef TAMAGOTCHI_LCD_H
#define TAMAGOTCHI_LCD_H

#include "../MPU.h"
#include "../Peripheral.h"

enum LCD_ICONS {
  INFO = 0,
  FOOD = 1,
  TOILETTE = 2,
  DOOR = 3,
  FIGURE = 4,
  TRAINING = 5,
  MEDICAL = 6,
  IR = 7,
  ALBUM = 8,
  ATTENTION = 9
};

class LCD : Peripheral {

public:
  LCD(MPU &mpu);

  bool checkAddress(uint16_t addr);

  void write(uint16_t addr, uint8_t value);
  uint8_t read(uint16_t addr);

  void update(unsigned int cycles);
  void reset();

  bool getIcon(unsigned short index) const;
  unsigned short getPixel(unsigned short x, unsigned short y) const;

private:
  uint8_t lcd_ram[512];

  bool _dirty;
};


#endif //TAMAGOTCHI_LCD_H
