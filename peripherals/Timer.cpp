//
// Created by Daniele on 03/12/2015.
//

#include "Timer.h"
#include "Interrupt.h"

#define freq(x) (unsigned int)(mpu.getFCPU() / x)

Timer::Timer(MPU &mpu) :
  Peripheral(mpu, "Timer"),
  // Timers 0 & 1 are overflow-based
  _timer0(0xFFFF), _timer1(0xFFFF),
  _tbh(0), _tbl(0),
  _t32k(freq(1000000L)), // Probably the 32k crystal is actually a 1-2MHz crystal
  _t2(freq(2)), _t8(freq(8)),
  _t32(freq(32)), _t64(freq(64)),
  _t2k(freq(2048)), _t8k(freq(8192)){ }

void Timer::reset() {
  _t0_source = 0;
  _t1_source = 0;

  _tbh.reset();
  _tbl.reset();

  // Update timers
  _t2.reset();
  _t8.reset();
  _t32.reset();
  _t64.reset();
  _t32k.reset();

  _t2k.reset();
  _t8k.reset();
}

bool Timer::checkAddress(uint16_t addr) {
  return (addr >= 0x3030 && addr <= 0x3035);
}

void Timer::write(uint16_t addr, uint8_t value) {
  //printf("[Timer] Writing to timer addr 0x%04x, value %d\n", addr, value);

  if (addr == 0x3030) {
    _tbl.target = (unsigned int)(_mpu.getFCPU() / TimeBaseLowDiv[(value >> 2) & 0x03]);
    _tbh.target = (unsigned int)(_mpu.getFCPU() / TimeBaseHighDiv[value & 0x03]);
  }else if (addr == 0x3031) {
    char source1 = (char)((value >> 2) & 0x07);
    char source2 = (char)((value >> 5) & 0x07);
    // 4 is the index for VDD source, see docs
    _t0_source = (char)(source2 > 0 ? source2 : source1 + 4);
    _t1_source = (char)(value & 0x03);
    printf("[Timer] Timer 0 source set to %d\n", _t0_source);
    printf("[Timer] Timer 1 source set to %d\n", _t1_source);
  } else if (addr == 0x3032) {
    _timer0.counter = (_timer0.counter & 0xFF00) + value;
  } else if (addr == 0x3033) {
    _timer0.counter = (value << 8) + (_timer0.counter & 0x00FF);
    //printf("[Timer] Timer 0 value set to %ld\n", _timer0.counter);
  } else if (addr == 0x3034) {
    _timer1.counter = (_timer1.counter & 0xFF00) + value;
  } else if (addr == 0x3035) {
    _timer1.counter = (value << 8) + (_timer1.counter & 0x00FF);
    //printf("[Timer] Timer 1 value set to %ld\n", _timer1.counter);
  }
}

uint8_t Timer::read(uint16_t addr) {
  //printf("[Timer] Reading from timer addr 0x%04x\n", addr);

  if (addr == 0x3030) {
    // TODO
    printf("[Timer] Read time base rates: not implemented yet.");
  } else if (addr == 0x3031) {
    // TODO: build register from _t0_source e _t1_source
    printf("[Timer] Read timer source: not implemented yet.");
  } else if (addr == 0x3032) {
    return (uint8_t)(_timer0.counter & 0x00FF);
  } else if (addr == 0x3033) {
    return (uint8_t)((_timer0.counter & 0xFF00) >> 8);
  } else if (addr == 0x3034) {
    return (uint8_t)(_timer1.counter & 0x00FF);
  } else if (addr == 0x3035) {
    return (uint8_t)((_timer1.counter & 0xFF00) >> 8);
  }

  return 0;
}

void Timer::update(unsigned int cycles) {
  // Update time bases
  _tbh.feed(cycles);
  _tbl.feed(cycles);

  // Update timers
  _t2.feed(cycles);
  _t8.feed(cycles);
  _t32.feed(cycles);
  _t64.feed(cycles);
  _t32k.feed(cycles);

  _t2k.feed(cycles);
  _t8k.feed(cycles);


  // Cycles mapped to source register values
  unsigned int t0_cycles[] = {
    // VSS, ROSC,   32K,         ECLK,   VDD
    0,   cycles, _t32k.ticks, cycles, 0,
    // TBL,       TBH,        EXTI
    _tbl.ticks, _tbh.ticks, 0,
    // 2HZ,       8HZ,      32HZ,       64HZ
    _t2.ticks, _t8.ticks, _t32.ticks, _t64.ticks
  };

  _timer0.feed(t0_cycles[_t0_source]);

  unsigned int t1_cycles[] = {
    // VSS, ROSC,   32K,         T0
    0,   cycles, _t32k.ticks, _timer0.ticks
  };

  _timer1.feed(t1_cycles[_t1_source]);

  if (_timer0.ticks) _mpu.interrupt(IRQ_TM0);
  if (_timer1.ticks) _mpu.interrupt(IRQ_TM1);
  if (_t64.ticks) _mpu.interrupt(IRQ_FP);
  if (_tbh.ticks) _mpu.interrupt(IRQ_TBH);
  if (_tbl.ticks) _mpu.interrupt(IRQ_TBL);
  if (_t2k.ticks) _mpu.interrupt(IRQ_2K);
  if (_t8k.ticks) _mpu.interrupt(IRQ_8K);
}
