//
// Created by Daniele Colanardi on 30/11/15.
//

#include <memory.h>

#include "LCD.h"

LCD::LCD(MPU &mpu) : Peripheral(mpu, "LCD") { }

void LCD::reset() {
  memset(lcd_ram, 0, 512);
  _dirty = true;
}


bool LCD::checkAddress(uint16_t addr) {
  return (addr >= 0x3040 && addr <= 0x304A) ||
      (addr >= 0x1000 && addr <= 0x11FF);
}


void LCD::write(uint16_t addr, uint8_t value) {
  // Register - not implemented
  if (addr > 0x3000) {
    //printf("[LCD] Register written: 0x%04x = 0x%02x\n", addr, value);
    return;
  }

  lcd_ram[addr - 0x1000] = value;

  // 0x100B is the last byte of the first row, where the status icon are stored
  // These icons are set after a full screen refresh, so that means that it is ready to be rendered
  if (addr <= 0x100B)
    _dirty = true;
}

uint8_t LCD::read(uint16_t addr) {
  // Register - not implemented
  if (addr > 0x3000)
    return 0;

  return lcd_ram[addr - 0x1000];
}

void LCD::update(unsigned int cycles) { }

bool LCD::getIcon(unsigned short index) const
{
  // Icons are treated like pixels in LCD RAM, in the 31st row,
  // from columns 19 to 28.
  // Given the odd row ordering in RAM, the 31st row actually starts at index 0
  // See getPixel() for more
  auto x = 19 + index;

  // Pixel position in ram byte
  auto shift = (3 - x % 4) * 2;
  auto pixel = (lcd_ram[x / 4] >> shift) & 0x03;

  return pixel ? true : false;
}

unsigned short LCD::getPixel(unsigned short x, unsigned short y) const
{
  if (x >= 48 || y >= 31)
    return 0;

  // VRAM is organized as such:
  //
  //   BYTE 1       BYTE 2       BYTE 3
  // xx xx xx xx  xx xx xx xx  xx xx xx xx ...
  // 0  1  2  3   4  5  6  7   8  9  10 11
  //
  // Each byte contains 4 pixels, row-major.

  // i is the linear index of the pixel in a 48*31 grid.
  // So i / 4 is the byte index in VRAM
  // and i % 4 is the actual pixel inside of the single byte.
  auto row = y < 16 ? y+16 : 31-y;
  auto i = x + 48*row;

  // Pixel position in ram byte.
  // Note that the first pixel in a byte is the left-most, so the byte must be shifted by 6,
  // while the fourth is the right-most, and the byte have not to be shifted
  auto pixel = (3 - i % 4) * 2;

  // Pixel value in gray-scale
  return (lcd_ram[i / 4] >> pixel) & 0x03;
}
