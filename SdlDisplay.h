#ifndef TAMAGOTCHI_SDLDISPLAY_H
#define TAMAGOTCHI_SDLDISPLAY_H

#include <bitset>
#include <SDL.h>

#include "peripherals/LCD.h"
#include "peripherals/IOPort.h"

class SDLDisplay
{
public:
  SDLDisplay(const LCD & lcd, IOPort & io);
  ~SDLDisplay();

  void init();
  void update();

private:
  const LCD & _lcd;
  IOPort & _io;

  SDL_Window* window;
  SDL_Renderer* renderer;

  unsigned int _lastTicks;
  std::bitset<5> _input_state;
};

#endif // TAMAGOTCHI_SDLDISPLAY_H
