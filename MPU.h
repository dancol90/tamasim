#ifndef TAMAGOTCHI_MPU_H
#define TAMAGOTCHI_MPU_H

#include <chrono>

#include "mos6502.h"
#include "Peripheral.h"

using namespace std;

class MPU : MPUCallbacks {

public:
  MPU();
  void addHardware(Peripheral *hw);
  void setInterruptHandler(InterruptPeripheral *irq);

  void reset();
  void run();

  void beginRun();
  void endRun();

  void interrupt(int irq_id);

  unsigned short getDivider() const { return _divider; }
  void setDivider(unsigned short divider);

  unsigned long getFCPU() { return FCPU; }

private:
  void BusWrite(uint16_t address, uint8_t value);
  uint8_t BusRead(uint16_t address);

  // Peripheral collection
  Peripheral** _hw;
  int _hw_size;
  InterruptPeripheral* _irq;

  // NOTE: A cycle = a call to run()

  /* IMPORTANT: Choose CPS, GRAN and FCPU such that
     *
     *  FCPU / (CPS*128) = n * GRAN   with n integer
     *
     * If the condition is not met, it will cause inaccurate timings.
     */

  // How many call to run() takes a second
  unsigned int CPS = 50;
  // How many mpu clock between peripheral updates
  unsigned int GRANULARITY = 125;
  // MPU clock frequency
  unsigned long FCPU = 16000000L;

  // MPU clock divider
  unsigned short _divider;
  // MPU clocks to perform in a run() call
  unsigned long _clock_per_iteration;

  mos6502 mpu;

  chrono::high_resolution_clock::time_point _start_time;
};


#endif //TAMAGOTCHI_MPU_H

class MPU;
