#include <thread>
#include <string>

#include "MPU.h"

MPU::MPU() : mpu(*this) {
  printf("[MPU] Created, FCPU=%ld CPS=%d GRANULARITY=%d\n", FCPU, CPS, GRANULARITY);

  _hw = new Peripheral*[20];
  _hw_size = 0;
}

void MPU::run() {
  beginRun();
  endRun();
}

void MPU::beginRun()
{
  // Start counting time
  _start_time = chrono::high_resolution_clock::now();
  unsigned long remaining_clocks = FCPU / CPS;

  unsigned int clocks = 0;

  while (remaining_clocks) {
    remaining_clocks -= GRANULARITY;

    // Run CPU (in GRAN steps)
    mpu.Run(_clock_per_iteration);

    // Update peripherals
    for (int i = 0; i < _hw_size; i++)
      _hw[i]->update(GRANULARITY);
  }
}

void MPU::endRun()
{
  // Stop counting time
  auto end = chrono::high_resolution_clock::now();
  auto elapsed_us = chrono::duration_cast<chrono::microseconds>(end - _start_time).count();

  // A cycle must take around 1/_FPS seconds. Multiply by 1e6 for microseconds.
  long long us = 1000000L / CPS - elapsed_us;

  //std::cout << "Frame took " << elapsed_us << ", expecting " << 1000000L / CPS << ", sleeping " << (us > 0 ? us : 0) << std::endl;

  // Sleeps for remaining cycle time
  if (us > 0)
    this_thread::sleep_for(chrono::microseconds(us));
}

void MPU::reset() {
  cout << "[MPU] Reset" << endl;

  mpu.Reset();

  for (int i = 0; i < _hw_size; i++)
    _hw[i]->reset();
}

uint8_t MPU::BusRead(uint16_t address) {
  for (int i = 0; i < _hw_size; i++)
    if (_hw[i]->checkAddress(address))
      return _hw[i]->read(address);

  printf("Invalid read address 0x%04x: no hw mapped\n", address);
  return 0;
}

void MPU::BusWrite(uint16_t address, uint8_t value) {
  for (int i = 0; i < _hw_size; i++)
    if (_hw[i]->checkAddress(address)) {
      _hw[i]->write(address, value);
      return;
    }

  printf("Invalid write address 0x%04x: no hw mapped (writig 0x%02x)\n", address, value);
}

void MPU::addHardware(Peripheral *hw) {
  cout << "[MPU] Adding mapped hardware: " << hw->getName() << endl;

  _hw[_hw_size++] = hw;
}


void MPU::setInterruptHandler(InterruptPeripheral *irq) {
  _irq = irq;
}


void MPU::setDivider(unsigned short divider)  {
  //cout << "[MPU] Set divider to " << divider << endl;
  _divider = divider;
  _clock_per_iteration = divider == 0 ? 0 : GRANULARITY / _divider;
}

void MPU::interrupt(int irq_id) {
  MPUInterruptType type = INT_NONE;
  unsigned int vector;

  // Get data
  if (_irq)
    _irq->trigger(irq_id, type, vector);

  if (type != INT_NONE)
    mpu.Interrupt(type, (uint16_t)vector);
}
