//
// Created by Daniele Colanardi on 29/11/15.
//

#ifndef TAMAGOTCHI_HARDWARE_H
#define TAMAGOTCHI_HARDWARE_H

#include "MPU.h"
#include "mos6502.h"

class Peripheral {
public:
  Peripheral(MPU &mpu, string name);

  virtual bool checkAddress(uint16_t addr) = 0;

  virtual void write(uint16_t addr, uint8_t value) = 0;
  virtual uint8_t read(uint16_t addr) = 0;

  virtual void update(unsigned int cycles) = 0;

  virtual void reset() = 0;

  const string& getName() const { return _name; }

protected:
  string _name;
  MPU &_mpu;
};

class InterruptPeripheral : protected Peripheral {
public:
  InterruptPeripheral(MPU &mpu, string name);

  virtual void trigger(int irq_id, MPUInterruptType& type, unsigned int& vector) = 0;
};


#endif //TAMAGOTCHI_HARDWARE_H

class Peripheral;
class InterruptPeripheral;
