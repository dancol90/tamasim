find_path(FMT_INCLUDE_DIR NAMES format.h
                          PATH_SUFFIXES fmt)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(FMT
                                  REQUIRED_VARS FMT_INCLUDE_DIR)

if(FMT_FOUND)
  set(FMT_LIBRARIES ${FMT_LIBRARY})
  set(FMT_INCLUDE_DIRS ${FMT_INCLUDE_DIR})

  set(FMT_DEFINES -DFMT_HEADER_ONLY)
endif()

mark_as_advanced(FMT_INCLUDE_DIR FMT_LIBRARY FMT_DEFINES)