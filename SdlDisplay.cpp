#include <iostream>
#include <map>

#include "SdlDisplay.h"

SDL_Color LCD_COLORS[] = {
  { 0x78, 0x9B, 0x83, 0 }, // Background

  { 0x91, 0xB4, 0x9C, 0 },
  { 0x5E, 0x81, 0x69, 0 },
  { 0x2B, 0x4E, 0x36, 0 },
  { 0x00, 0x1B, 0x03, 0 },
};

std::map<SDL_Keycode, unsigned int> BUTTON_MAP = {
  { SDLK_z, 0 },
  { SDLK_x, 1 },
  { SDLK_c, 2 },
  { SDLK_r, 3 }
};

SDLDisplay::SDLDisplay(const LCD & lcd, IOPort & io)
  : _lcd(lcd), _io(io)
{
}

SDLDisplay::~SDLDisplay() {
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
}

void SDLDisplay::init() {
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    std::cout << "Unable to init SDL:" << SDL_GetError() << std::endl;
    exit(1);
  }

  window = SDL_CreateWindow("Tamagotchi!", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 48 * 5, 32 * 5, SDL_WINDOW_SHOWN);

  if (!window) {
    std::cout << "Unable to create SDL window:" << SDL_GetError() << std::endl;
    exit(2);
  }

  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

  if (!window) {
    SDL_DestroyWindow(window);
    std::cout << "Unable to create SDL renderer:" << SDL_GetError() << std::endl;
    exit(3);
  }
}

void SDLDisplay::update()
{
  /*unsigned int now = SDL_GetTicks();
  bool ret = (now - _lastTicks) > 40;

  _lastTicks = now;
  return ret;*/

  static long sum = 0;
  static unsigned long count = 0;
  auto start = std::chrono::high_resolution_clock::now();

  SDL_Color& color = LCD_COLORS[0];
  SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
  SDL_RenderClear(renderer);

  for (unsigned short y = 0; y < 31; y++)
  for (unsigned short x = 0; x < 48; x++)
  {
    auto level = _lcd.getPixel(x, y);

    SDL_Rect rect {
      static_cast<int>(x * 5), // x
      static_cast<int>(y * 5), // y
      4, 4 // w, h
    };

    SDL_Color& color = LCD_COLORS[level + 1];
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    SDL_RenderFillRect(renderer, &rect);
  }

  for (unsigned short i = 0; i < 10; i++)
  {
    auto enabled = _lcd.getIcon(i);

    SDL_Rect rect {
      static_cast<int>(i * 24), // x
      31 * 5,     // y
      24, 5       // w, h
    };

    SDL_Color& color = LCD_COLORS[enabled ? 4 : 1];
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    SDL_RenderFillRect(renderer, &rect);
  }

  SDL_RenderPresent(renderer);

  // TODO update inputs here

  SDL_Event event;

  while (SDL_PollEvent(&event)) {
    switch (event.type) {
      case SDL_QUIT:
        //exit = true;
        std::cout << (float)sum / count << std::endl;
        std::exit(0);
        break;

      case SDL_KEYDOWN:
      case SDL_KEYUP:
        auto iter = BUTTON_MAP.find(event.key.keysym.sym);

        if (iter != BUTTON_MAP.end())
        {
          std::cout << "Key!" << std::endl;
          _io.setButton(iter->second, event.type == SDL_KEYDOWN);
        }
        break;
    }
  }

  // Stop counting time
  auto end = std::chrono::high_resolution_clock::now();
  auto elapsed_us = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();

  sum += elapsed_us;
  count++;
}
