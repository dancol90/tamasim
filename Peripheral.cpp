//
// Created by Daniele Colanardi on 29/11/15.
//

#include "Peripheral.h"

Peripheral::Peripheral(MPU &mpu, string name): _name(name), _mpu(mpu) {
  mpu.addHardware(this);
}


InterruptPeripheral::InterruptPeripheral(MPU &mpu, string name) : Peripheral(mpu, name) {
  mpu.setInterruptHandler(this);
}


/*void Peripheral::write(uint16_t addr, uint8_t value) {
    printf("[%s] Writing 0x%02x address 0x%04x\n", _name.c_str(), value, addr);
}

uint8_t Peripheral::read(uint16_t addr) {
    printf("[%s] Reading address 0x%04x\n", _name.c_str(), addr);
    return 0;
}*/
